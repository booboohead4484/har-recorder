import os
from setuptools import setup, find_packages

cwd = os.getcwd()
try:
    os.chdir(os.path.dirname(os.path.realpath(__file__)))

    setup(
        name='gitlab-har-recorder',
        description='GitLab HAR Recording Tool',
        long_description=open('README.rst').read(),
        author='GitLab',
        url='https://gitlab.com/gitlab-org/security-products/har-recorder',
        version='0.0.0',
        python_requires='~=3.6',

        include_package_data=True,
        packages=find_packages('src'),
        package_dir={'': 'src'},
        
        eager_resources=[
            'gitlabrecorder/har_dump.py',
        ],

        # update requirements.txt as well!
        install_requires=[
            'click~=6.7', 
            'mitmproxy~=4.0.4',
            ],
        entry_points={
            'console_scripts': ['gl-har-recorder=gitlabrecorder.main:run'],
        },

        license='MIT',
        keywords='gitlab har recorder peach api fuzzing security test rest',

        classifiers=[
            'Development Status :: 5 - Production/Stable',
            'Intended Audience :: Developers',
            'License :: OSI Approved :: MIT Licens',
            'Operating System :: OS Independent',
            'Topic :: Security',
            'Topic :: Software Development :: Quality Assurance',
            'Topic :: Software Development :: Testing',
            'Topic :: Utilities',
            'Programming Language :: Python',
            'Programming Language :: Python :: 3'
        ])

finally:
    os.chdir(cwd)

# end
